#include <string>

std::string stringArrayTostring(const char *arrayCmd[]);
void appendForward(const char *interface, const char *ipAddress);
void insertPreroutingDNAT(const char *hostInterface, const char *ipAddress,
                          const char *TransportProtocol, const char *hostPort,
                          const char *guestPort);
