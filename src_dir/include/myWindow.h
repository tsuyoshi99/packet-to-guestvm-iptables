#include "gtkmm.h"

class MyWindow : public Gtk::Window {
 public:
  MyWindow();
  virtual ~MyWindow();
  void onApplyClicked();

 protected:
  // Child widgets:
  Gtk::Frame m_frame;

  Gtk::VBox m_vbox, m_configVbox;
  Gtk::ButtonBox m_buttonBox;

  Gtk::Button m_applyButton, m_quitButton;

  Gtk::Entry m_ipEntry, m_hostPortEntry, m_guestPortEntry, m_hostInterfaceEntry,
      m_guestInterfaceEntry;

  Gtk::Label m_versionLabel, m_ipLabel, m_hostPortLobel, m_guestPortLabel,
      m_interfaceLabel;

 private:
  void onQuitButtonClicked();
  void onApplyButtonClicked();
};

bool verifyInput(const char *ip, const char *hostPort, const char *guestPort);
