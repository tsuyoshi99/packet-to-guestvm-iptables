link_directories(${GTKMM_LIBRARY_DIRS})
include_directories(${GTKMM_INCLUDE_DIRS})

set(source_dir "${PROJECT_SOURCE_DIR}/src_dir/src/")

file (GLOB source_files "${source_dir}/*.cpp")

add_executable(helloWorld ${source_files}) 

target_link_libraries(helloWorld ${GTKMM_LIBRARIES})
