#include <regex>
#include <string>

bool verifyInput(const char *ip, const char *hostPort, const char *guestPort) {
  std::regex portRegex("^[0-9]{1,5}$", std::regex_constants::ECMAScript |
                                           std::regex_constants::icase);
  std::regex ipRegex("^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$",
                     std::regex_constants::ECMAScript);
  if (std::regex_search(hostPort, portRegex) &&
      std::regex_search(guestPort, portRegex) &&
      std::regex_search(ip, ipRegex)) {
    return true;
  }
  return false;
}
