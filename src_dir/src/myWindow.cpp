#include "../include/myWindow.h"

#include <iostream>
#include <regex>

#include "../include/config.h"
#include "../include/iptables.h"
#include "../include/utils.h"

MyWindow::MyWindow()
    : m_versionLabel("1.0.0-alpha"),
      m_quitButton("Quit"),
      m_applyButton("Apply"),
      m_ipLabel("IP Address", Gtk::ALIGN_START),
      m_hostPortLobel("Host Port", Gtk::ALIGN_START),
      m_guestPortLabel("Guest Port", Gtk::ALIGN_START),
      m_interfaceLabel("Interface", Gtk::ALIGN_START) {
  set_title("packetToGuestVM");
  set_size_request(500, 300);
  set_border_width(10);

  add(m_vbox);
  m_vbox.pack_start(m_frame);
  m_vbox.pack_start(m_versionLabel, Gtk::PACK_SHRINK);

  m_frame.add(m_configVbox);
  m_frame.set_label("Config");
  m_frame.set_label_align(Gtk::ALIGN_START, Gtk::ALIGN_END);

  m_ipEntry.set_placeholder_text(PLACEHOLDER_IP_ADDRESS);
  m_hostPortEntry.set_placeholder_text(PLACEHOLDER_PORT);
  m_guestPortEntry.set_placeholder_text(PLACEHOLDER_PORT);
  m_hostInterfaceEntry.set_placeholder_text(PLACEHOLDER_INTERFACE);
  m_guestInterfaceEntry.set_placeholder_text(PLACEHOLDER_INTERFACE);

  m_configVbox.set_margin_left(10);
  m_configVbox.set_margin_right(10);
  m_configVbox.pack_start(m_ipLabel, Gtk::PACK_SHRINK);
  m_configVbox.pack_start(m_ipEntry, Gtk::PACK_SHRINK);
  m_configVbox.pack_start(m_hostPortLobel, Gtk::PACK_SHRINK);
  m_configVbox.pack_start(m_hostPortEntry, Gtk::PACK_SHRINK);
  m_configVbox.pack_start(m_guestPortLabel, Gtk::PACK_SHRINK);
  m_configVbox.pack_start(m_guestPortEntry, Gtk::PACK_SHRINK);
  m_configVbox.pack_start(m_interfaceLabel, Gtk::PACK_SHRINK);
  m_configVbox.pack_start(m_hostInterfaceEntry, Gtk::PACK_SHRINK);
  m_configVbox.pack_start(m_guestInterfaceEntry, Gtk::PACK_SHRINK);
  m_configVbox.pack_start(m_buttonBox);

  m_buttonBox.pack_start(m_applyButton, Gtk::PACK_SHRINK);
  m_buttonBox.pack_start(m_quitButton, Gtk::PACK_SHRINK);
  m_buttonBox.set_spacing(5);
  m_buttonBox.set_layout(Gtk::BUTTONBOX_END);

  m_quitButton.signal_clicked().connect(
      sigc::mem_fun(*this, &MyWindow::onQuitButtonClicked));
  m_applyButton.signal_clicked().connect(
      sigc::mem_fun(*this, &MyWindow::onApplyButtonClicked));

  show_all_children();
};

void MyWindow::onQuitButtonClicked() { hide(); }

void MyWindow::onApplyButtonClicked() {
  if (!verifyInput(m_ipEntry.get_text().c_str(),
                   m_hostPortEntry.get_text().c_str(),
                   m_guestPortEntry.get_text().c_str())) {
    Gtk::MessageDialog wrongInputDialog("Your inputs is not correct",
                                        Gtk::MESSAGE_WARNING);
    wrongInputDialog.run();
    return;
  }

  appendForward(m_guestInterfaceEntry.get_text().c_str(),
                m_ipEntry.get_text().c_str());
  insertPreroutingDNAT(m_hostInterfaceEntry.get_text().c_str(),
                       m_ipEntry.get_text().c_str(), "tcp",
                       m_hostPortEntry.get_text().c_str(),
                       m_guestPortEntry.get_text().c_str());
  Gtk::MessageDialog successDialog("Successful", Gtk::MESSAGE_INFO);
  successDialog.run();
}

MyWindow::~MyWindow() {}
