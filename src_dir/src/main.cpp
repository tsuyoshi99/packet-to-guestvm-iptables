#define VERSION "1.0.0-prealpha"

#include <iostream>

#include "../include/iptables.h"
#include "../include/myWindow.h"
#include "gtkmm/application.h"

int main(int argc, char* argv[]) {
  auto app = Gtk::Application::create(argc, argv, "org.vikran.packetToGuestVM");

  MyWindow window;

  return app->run(window);
}
