#include "../include/iptables.h"

#include <cstdlib>
#include <iostream>
#include <string>

std::string stringArrayTostring(const char *arrayCmd[]) {
  int i = 0;
  std::string cmd = "";
  while (arrayCmd[i] != NULL) {
    cmd.append(arrayCmd[i]);
    i++;
  }
  return cmd;
}

void appendForward(const char *interface, const char *ipAddress) {
  const char *arrayCmd[] = {"iptables -I FORWARD -o ",
                            interface,
                            " -d ",
                            ipAddress,
                            " -j ACCEPT",
                            NULL};
  std::string cmd = stringArrayTostring(arrayCmd);
  std::cout << cmd << std::endl;
  //system(cmd.c_str());
}

void insertPreroutingDNAT(const char *hostInterface, const char *ipAddress,
                          const char *TransportProtocol, const char *hostPort,
                          const char *guestPort) {
  const char *arrayCmd[] = {"iptables -t nat -I PREROUTING -i ",
                            hostInterface,
                            " -p ",
                            TransportProtocol,
                            " --dport ",
                            hostPort,
                            " -j DNAT --to-destination ",
                            ipAddress,
                            ":",
                            guestPort,
                            NULL};
  std::string cmd = stringArrayTostring(arrayCmd);
  std::cout << cmd << std::endl;
  //system(cmd.c_str());
}
