# packet-to-guestVM-iptables

GUI application using GTK which use iptables to forward packet from outside Host to Guest Virtual Machine (KVM/Virsh)

#Version: 1.0.0-prealpha

#Target Platform: Linux

## Must-have

```
gtkmm
iptables
```

## Compile & Run

```bash
cmake .
cmake --build .
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)
